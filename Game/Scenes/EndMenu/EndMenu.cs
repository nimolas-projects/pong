﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pong.Engine;

namespace Pong.Game.Scenes
{
    public class EndMenu : BaseMenu
    {
        readonly SpriteFont menuFont;

        public EndMenu(GameSceneScores scores) : base()
        {
            this.scores = scores;

            menuFont = BaseGame.LoadContent<SpriteFont>("Font");
            textObjects.Add(new TextObject(menuFont, "Game Over! Press:", new Vector2(20, 75), Color.Green));
            textObjects.Add(new TextObject(menuFont, "<Space> to play against a player.", new Vector2(20, 240), Color.Green));
            textObjects.Add(new TextObject(menuFont, "<D> for demo.", new Vector2(20, 290), Color.Green));
            textObjects.Add(new TextObject(menuFont, "<Enter> to player against an ai.", new Vector2(20, 340), Color.Green));
        }
    }
}
