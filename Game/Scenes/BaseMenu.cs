﻿using Microsoft.Xna.Framework.Input;
using Pong.Engine;


namespace Pong.Game.Scenes
{
    public class BaseMenu : Scene
    {
        protected GameSceneScores scores = new(0, 0);
        void CheckInput()
        {
            if (Input.GetKeyDown(Keys.Space))
            {
                var gameScene = new GameScene(new GameSceneArgs(scores, playerVersusPlayer: true));
                BaseGame.SetNextScene(gameScene);
            }
            else if (Input.GetKeyDown(Keys.D))
            {
                var gameScene = new GameScene(new GameSceneArgs(scores, aiVersusAi: true));
                BaseGame.SetNextScene(gameScene);
            }
            else if (Input.GetKeyDown(Keys.Enter))
            {
                var gameScene = new GameScene(new GameSceneArgs(scores, playerVersusAi: true));
                BaseGame.SetNextScene(gameScene);
            }
        }

        public override void Update()
        {
            CheckInput();
        }
    }
}
