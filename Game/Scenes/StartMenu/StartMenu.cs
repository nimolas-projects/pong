﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pong.Engine;

namespace Pong.Game.Scenes
{
    public class StartMenu : BaseMenu
    {
        readonly SpriteFont menuFont;
        public StartMenu() : base()
        {
            menuFont = BaseGame.LoadContent<SpriteFont>("Font");
            textObjects.Add(new TextObject(menuFont, "Welcome to Pong! Press:", new Vector2(20, 100), Color.Black));
            textObjects.Add(new TextObject(menuFont, "<Space> to play against a player.", new Vector2(20, 160), Color.Black));
            textObjects.Add(new TextObject(menuFont, "<D> for demo.", new Vector2(20, 220), Color.Black));
            textObjects.Add(new TextObject(menuFont, "<Enter> to player against an ai.", new Vector2(20, 280), Color.Black));
        }
    }
}
