﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Pong.Engine;
using Pong.Game.Scenes.GameObjects;
using System.Linq;

namespace Pong.Game.Scenes
{
    public struct GameSceneScores
    {
        public int Player1Score;
        public int Player2Score;

        public GameSceneScores(int player1Score, int player2Score)
        {
            Player1Score = player1Score;
            Player2Score = player2Score;
        }
    }

    public struct GameSceneArgs
    {
        public bool AiVersusAi;
        public bool PlayerVersusAi;
        public bool PlayerVersusPlayer;
        public GameSceneScores Scores;

        public GameSceneArgs(GameSceneScores scores, bool aiVersusAi = false, bool playerVersusAi = false, bool playerVersusPlayer = false)
        {
            AiVersusAi = aiVersusAi;
            PlayerVersusAi = playerVersusAi;
            PlayerVersusPlayer = playerVersusPlayer;
            Scores = scores;
        }
    }


    public class GameScene : Scene
    {
        GameSceneScores scores;
        public GameScene(GameSceneArgs args) : base()
        {
            scores = args.Scores;

            var player1Texture = BaseGame.LoadContent<Texture2D>("rodeSpeler");
            var player2Texture = BaseGame.LoadContent<Texture2D>("blauweSpeler");
            var ballTexture = BaseGame.LoadContent<Texture2D>("bal");
            var scoreFont = BaseGame.LoadContent<SpriteFont>("font");

            var player1Position = new Vector2(20, (BaseGame.screenHeight / 2) - (player1Texture.Height / 2));
            var player2Position = new Vector2(BaseGame.screenWidth - 20 - player2Texture.Width, (BaseGame.screenHeight / 2) - (player2Texture.Height / 2));
            var ballPosition = new Vector2((BaseGame.screenWidth / 2) - (ballTexture.Width / 2), (BaseGame.screenHeight / 2) - (ballTexture.Height / 2));

            var player1MovementKeys = new MovementKeys(Keys.W, Keys.S);
            var player2MovementKeys = new MovementKeys(Keys.Up, Keys.Down);

            for (var lifes = 0; lifes < 3; lifes++)
            {
                gameObjects.Add(new Life(ballTexture, new Vector2(5 + (lifes * 20), 5), PlayerLife.Player1));
                gameObjects.Add(new Life(ballTexture, new Vector2(780 - (lifes * 20), 5), PlayerLife.Player2));
            }

            textObjects.Add(new TextObject(scoreFont, scores.Player1Score.ToString(), new Vector2(320, 20), Color.Red));
            textObjects.Add(new TextObject(scoreFont, scores.Player2Score.ToString(), new Vector2(440, 20), Color.Blue));

            if ((bool)args.AiVersusAi)
            {
                gameObjects.Add(new Paddle(player1Texture, player1Position, true, player1MovementKeys));
                gameObjects.Add(new Paddle(player2Texture, player2Position, true, player2MovementKeys));
                gameObjects.Add(new Ball(ballTexture, ballPosition));
            }
            else if ((bool)args.PlayerVersusAi)
            {
                gameObjects.Add(new Paddle(player1Texture, player1Position, false, player1MovementKeys));
                gameObjects.Add(new Paddle(player2Texture, player2Position, true, player2MovementKeys));
                gameObjects.Add(new Ball(ballTexture, ballPosition));
            }
            else if ((bool)args.PlayerVersusPlayer)
            {
                gameObjects.Add(new Paddle(player1Texture, player1Position, false, player1MovementKeys));
                gameObjects.Add(new Paddle(player2Texture, player2Position, false, player2MovementKeys));
                gameObjects.Add(new Ball(ballTexture, ballPosition));
            }
        }

        public override void Update()
        {
            var livesRemaining = gameObjects.FindAll(gameObject => gameObject is Life);
            var player1Lives = livesRemaining.Count(gameObject => (gameObject as Life).PlayerLife == PlayerLife.Player1);
            var player2Lives = livesRemaining.Count(gameObject => (gameObject as Life).PlayerLife == PlayerLife.Player2);

            if (player1Lives == 0)
            {
                BaseGame.SetNextScene(new EndMenu(new(scores.Player1Score, scores.Player2Score += 1)));
            }

            if (player2Lives == 0)
            {
                BaseGame.SetNextScene(new EndMenu(new(scores.Player1Score += 1, scores.Player2Score)));
            }

            base.Update();
        }
    }
}
