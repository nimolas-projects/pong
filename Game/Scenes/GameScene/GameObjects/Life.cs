﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pong.Engine;

namespace Pong.Game.Scenes.GameObjects
{
    public enum PlayerLife
    {
        Player1,
        Player2,
    }

    public class Life : GameObject
    {
        readonly PlayerLife playerLife;
        public PlayerLife PlayerLife { get { return playerLife; } }
        public Life(Texture2D texture, Vector2 position, PlayerLife playerLife) : base(texture, position)
        {
            this.playerLife = playerLife;
        }
    }
}
