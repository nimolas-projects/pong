﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Pong.Engine;
using System.Collections.Generic;

namespace Pong.Game.Scenes.GameObjects
{
    public struct MovementKeys
    {
        public Keys Up;
        public Keys Down;

        public MovementKeys(Keys up, Keys down)
        {
            Up = up;
            Down = down;
        }
    }

    public class Paddle : GameObject
    {
        readonly bool isAi;
        readonly float speed;
        MovementKeys keys;
        readonly float aiXOffset;

        public Paddle(Texture2D texture, Vector2 position, bool isAi, MovementKeys keys) : base(texture, position)
        {
            this.isAi = isAi;
            this.keys = keys;

            aiXOffset = position.X > BaseGame.screenWidth / 2 ? -(BaseGame.screenWidth / 2) : BaseGame.screenWidth / 2;

            speed = 3f;
        }

        void MovePaddleUp()
        {
            position.Y -= speed;
        }

        void MovePaddleDown()
        {
            position.Y += speed;
        }

        void ClampPosition()
        {
            position.Y = MathHelper.Clamp(position.Y, 25, BaseGame.screenHeight - texture.Height);
        }

        void MovePlayer()
        {
            if (Input.GetKeyDown(keys.Up)) MovePaddleUp();

            if (Input.GetKeyDown(keys.Down)) MovePaddleDown();

            ClampPosition();
        }

        bool IsInPlayerArea(Ball ball)
        {
            return (ball.Position.X > position.X && ball.Position.X < position.X + aiXOffset) ||
                (ball.Position.X > position.X + aiXOffset && ball.Position.X < position.X);
        }

        void MoveAi(Ball ball)
        {
            if (IsInPlayerArea(ball))
            {
                if (ball.Position.Y > position.Y + texture.Height / 2) MovePaddleDown();
                if (ball.Position.Y < position.Y + texture.Height / 2) MovePaddleUp();
            }

            ClampPosition();
        }

        public override void Update(List<GameObject> gameObjects)
        {
            if (isAi) MoveAi(gameObjects.Find(gameObject => gameObject is Ball) as Ball);
            else MovePlayer();

            base.Update(gameObjects);
        }
    }
}
