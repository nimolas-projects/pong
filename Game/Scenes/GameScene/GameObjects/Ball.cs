﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pong.Engine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pong.Game.Scenes.GameObjects
{
    public class Ball : GameObject
    {
        float speed;
        Vector2 direction;
        bool positiveDirectionY = true;
        bool positiveDirectionX = true;
        bool alreadyIntersected = false;
        GameObject lastIntersectedPaddle;
        Vector2 originalPosition;
        readonly Random rand = new();

        public Vector2 Position { get { return position; } }

        public Ball(Texture2D texture, Vector2 position) : base(texture, position)
        {
            speed = 3f;
            originalPosition = position;

            SetDirection();
        }

        void ResetPosition()
        {
            speed = 3f;
            position = originalPosition;
        }

        void SetDirection()
        {
            var xOffset = rand.NextDouble() > 0.5 ? 1 : 0;
            var yOffset = rand.NextDouble() > 0.5 ? 1 : 0;

            direction = new Vector2(xOffset - (float)rand.NextDouble(), yOffset - (float)rand.NextDouble());
            direction.Normalize();
        }

        void MoveBall()
        {
            position.Y += positiveDirectionY ? direction.Y * speed : -(direction.Y * speed);
            position.X += positiveDirectionX ? direction.X * speed : -(direction.X * speed);
        }

        void IncreaseSpeed()
        {
            speed += 0.2f;
        }

        static void RemovePlayerLife(List<GameObject> lives)
        {
            if (lives.Count > 0)
            {
                lives.ElementAt(0).ToDelete = true;
            }
        }

        void CheckWindowBoundaries(List<GameObject> gameObjects)
        {
            var pastXBoundary = false;

            if (position.Y > BaseGame.screenHeight - texture.Height || position.Y < 25) positiveDirectionY = !positiveDirectionY;

            if (position.X > BaseGame.screenWidth + texture.Width / 2)
            {
                var player2Lifes = gameObjects.FindAll(gameObject => gameObject is Life).FindAll(life => (life as Life).PlayerLife == PlayerLife.Player2);
                RemovePlayerLife(player2Lifes);
                pastXBoundary = true;
            }

            if (position.X < 0 - texture.Width / 2)
            {
                var player1Lifes = gameObjects.FindAll(gameObject => gameObject is Life).FindAll(life => (life as Life).PlayerLife == PlayerLife.Player1);
                RemovePlayerLife(player1Lifes);
                pastXBoundary = true;
            }

            if (pastXBoundary)
            {
                ResetPosition();
                SetDirection();
            }
        }

        void CheckPaddleBoundaries(List<GameObject> paddles)
        {
            if (!alreadyIntersected)
            {
                if (Intersects(paddles.ElementAt(0)) || Intersects(paddles.ElementAt(1)))
                {
                    lastIntersectedPaddle = Intersects(paddles.ElementAt(0)) ? paddles.ElementAt(0) : paddles.ElementAt(1);

                    positiveDirectionX = !positiveDirectionX;
                    IncreaseSpeed();

                    alreadyIntersected = !alreadyIntersected;
                }
            }
            else if (!Intersects(lastIntersectedPaddle))
            {
                alreadyIntersected = !alreadyIntersected;
            }
        }

        public override void Update(List<GameObject> gameObjects)
        {
            MoveBall();

            CheckWindowBoundaries(gameObjects);

            CheckPaddleBoundaries(gameObjects.FindAll(gameObject => gameObject is Paddle));

            base.Update(gameObjects);
        }
    }
}
