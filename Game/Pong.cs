﻿using Pong.Engine;
using Pong.Game.Scenes;

namespace Pong.Game
{
    public class Pong : BaseGame
    {
        protected override void SetupGame()
        {
            scenes.Add(new StartMenu());
        }
    }
}
