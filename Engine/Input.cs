﻿using Microsoft.Xna.Framework.Input;

namespace Pong.Engine
{
    public static class Input
    {
        public static bool GetKeyDown(Keys key)
        {
            return Keyboard.GetState().IsKeyDown(key);
        }
    }
}
