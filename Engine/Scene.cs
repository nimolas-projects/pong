﻿using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Pong.Engine
{
    public class Scene
    {
        protected List<GameObject> gameObjects = new();
        protected List<TextObject> textObjects = new();

        public Scene()
        {
        }

        void CheckToDelete()
        {
            foreach (var gameObject in gameObjects)
            {
                if (gameObject.ToDelete)
                {
                    gameObjects.Remove(gameObject);
                    break;
                }
            }
        }

        public virtual void Update()
        {
            gameObjects.ForEach(gameObject => gameObject.Update(gameObjects));
            textObjects.ForEach(textObject => textObject.Update());

            CheckToDelete();
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            gameObjects.ForEach(gameObject => gameObject.Draw(spriteBatch));
            textObjects.ForEach(textObject => textObject.Draw(spriteBatch));
        }
    }
}
