﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Pong.Engine
{
    public class GameObject
    {
        protected Texture2D texture;
        protected bool toDelete = false;
        public bool ToDelete { get { return toDelete; } set { toDelete = value; } }
        public Rectangle ObjectBounds
        {
            get { return objectBounds; }
        }
        protected Rectangle objectBounds;
        protected Vector2 position;

        public GameObject(Texture2D texture, Vector2 position)
        {
            this.texture = texture;
            this.position = position;

            objectBounds = new(new Point((int)position.X, (int)position.Y), new Point(texture.Width, texture.Height));
        }
        public virtual void Update(List<GameObject> gameObjects)
        {
            objectBounds = new(new Point((int)position.X, (int)position.Y), new Point(texture.Width, texture.Height));
        }
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }

        public bool Intersects(GameObject otherGameObject)
        {
            return objectBounds.Intersects(otherGameObject.ObjectBounds);
        }
    }
}
