﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Linq;

namespace Pong.Engine
{
    public class BaseGame : Microsoft.Xna.Framework.Game
    {
        public static int screenHeight;
        public static int screenWidth;
        public static Scene activeScene = null;
        protected List<Scene> scenes = new();
        bool runGame;
        readonly GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        static ContentManager content;

        public BaseGame() : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            Window.IsBorderless = false;
            Window.AllowAltF4 = true;
            content = Content;
        }

        public List<Scene> GetScenes()
        {
            return scenes;
        }

        protected virtual void SetupGame()
        {

        }

        protected override void Initialize()
        {
            screenWidth = graphics.PreferredBackBufferWidth;
            screenHeight = graphics.PreferredBackBufferHeight;

            spriteBatch = new SpriteBatch(GraphicsDevice);

            SetupGame();

            activeScene ??= scenes.ElementAt(0);

            base.Initialize();
        }

        public new void Run()
        {
            runGame = true;

            base.Run();
        }

        public static void SetNextScene(Scene nextScene)
        {
            activeScene = nextScene;
        }
        public static T LoadContent<T>(string asset)
        {
            return content.Load<T>(asset);
        }

        protected override void Update(GameTime gameTime)
        {
            if (runGame)
            {
                activeScene.Update();
            }
        }


        protected override void Draw(GameTime gameTime)
        {
            if (runGame)
            {
                graphics.GraphicsDevice.Clear(Color.White);

                // TODO: Add your drawing code here

                spriteBatch.Begin();

                activeScene.Draw(spriteBatch);

                spriteBatch.End();

                base.Draw(gameTime);
            }
        }
    }
}
