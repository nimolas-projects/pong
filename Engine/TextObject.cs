﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Pong.Engine
{
    public class TextObject
    {
        protected SpriteFont Font { get; set; } = null;
        protected string Text { get; set; } = "";

        protected Vector2 Position { get; set; } = new(0, 0);
        Color Colour { get; set; } = new(new Vector3(0, 0, 0));

        public TextObject(SpriteFont font, string text, Vector2 position, Color colour)
        {
            this.Font = font;
            this.Text = text;
            this.Position = position;
            this.Colour = colour;
        }

        public void Update()
        {

        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Font, Text, Position, Colour);
        }
    }
}
